var takeShot = function() {
    var phantom = require('phantom');

    phantom.create(function (ph) {
        ph.createPage(function (page) {
            page.open("http://www.gamecomtools.com/api/twitchOverlay", function (status) {
                page.render('/dev/stdout', {format: "png"});
                ph.exit();
                setTimeout(takeShot, 1000);
            });
        });
    });
};

takeShot();
