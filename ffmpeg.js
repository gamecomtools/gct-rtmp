module.exports = function(streamName) {
    var ffmpeg = require('fluent-ffmpeg');

    var cl = function(){
        console.log(arguments);
    };

    var ret = null;

    function go() {
        return ffmpeg('rtmp://localhost/live/' + streamName)
            .inputFPS(29.7)
            .outputOptions('-preset fast')
            .videoCodec('libx264')
            .audioCodec('copy')
            .input('/tmp/overlay_' + streamName + '.png')
            .complexFilter([
                "[0:v][1:v] overlay=0:0"
            ])
            .output('rtmp://live.justin.tv/app/live_sub_c2e77ab639abcb5b02a7cd63a790b1c0d3078bc5')
            .fps(29.7)
            .format('flv');
    }

    ret = go();
    return ret;

    //ffmpeg -i rtmp://localhost/live/test -i /tmp/overlay_test.png -filter_complex "[0:v][1:v] overlay=50:50" -c:v libx264 -pix_fmt yuv420p -preset veryfast -c:a copy -f flv rtmp://live.justin.tv/app/live_sub_c2e77ab639abcb5b02a7cd63a790b1c0d3078bc5
};
